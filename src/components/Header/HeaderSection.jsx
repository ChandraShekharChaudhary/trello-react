import { useNavigate } from "react-router-dom";

import { Search } from "@mui/icons-material";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  InputBase,
  Button,
} from "@mui/material";

export default function HeaderSection() {
  let navi = useNavigate();
  return (
    <AppBar position="static" sx={{ backgroundColor: "#9fa2d4" }}>
      <Toolbar sx={{gap:'1rem'}}>
        <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
          <Button
            onClick={() => {
              navi("/");
            }}
            variant="contained"
            className="img-1"
            sx={{ color: "white" }}
            startIcon={
              <img src="/trello.png" alt="Logo" className="headerIcon" />
            }
          >
            Trello
          </Button>
        </Typography>
        <div className="searchContainer">
          <IconButton color="inherit">
            <Search />
          </IconButton>
          <InputBase
            placeholder="Search..."
            inputProps={{ "aria-label": "search" }}
          />
        </div>
      </Toolbar>
    </AppBar>
  );
}
