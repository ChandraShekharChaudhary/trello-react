import { Container, Typography } from "@mui/material";
import { Error } from "@mui/icons-material";

export default function ErrorHandler({ message }) {
  return <Container
    sx={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      fontSize: "5rem",
      gap: "1rem",
      height: "70vh",
    }}
  >
    <Error fontSize="100px" color="error" />
    <Typography variant="h4" color="error">
      {message}
    </Typography>
  </Container>;
}
