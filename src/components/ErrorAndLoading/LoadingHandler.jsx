import { LoadingButton } from "@mui/lab";
import { Container, Typography } from "@mui/material";

export default function LoadingHandler({ hType, alignment, height, padding }) {
  return (
    <Container
      sx={{
        textAlign: `${alignment || "center"}`,
        height: `${height || "10rem"}`,
        paddingTop: `${padding || "5rem"}`,
      }}
    >
      <Typography variant={hType || "h5"} color="primary">
        <LoadingButton color="secondary" loading={true} />
        <span>Loading...</span>
      </Typography>
    </Container>
  );
}
