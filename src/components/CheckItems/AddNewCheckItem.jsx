import { useState } from "react";

import { Add, Close } from "@mui/icons-material";
import { Button, Grid, Paper, TextField } from "@mui/material";

import { addNewCheckItem } from "../../TrelloAPI/TrelloAPI";

const checkItemStyle = {
  padding: 10,
  fontSize: 16,
  maxWidth: "300px",
  display: "flex",
  flexDirection: "row",
  gap: "10px",
  backgroundColor: "#fafafa",
};
export default function AddNewCheckItem({
  allCheckItems,
  setAllCheckItems,
  checklistID,
  setError,
}) {
  const [clicked, setClicked] = useState(false);
  const [event, setEvent] = useState(null);

  function addCheckItemButtonHandler() {
    if (event === null || event.target.value == "") {
      return;
    }

    addNewCheckItem(event.target.value, checklistID)
      .then((res) => {
        setAllCheckItems([...allCheckItems, res.data]);
        event.target.value = "";
      })
      .catch((err) => {
        setError(err.message)
        console.log(err.message);
      });
  }

  return (
    <Grid item xs={12} key="add-new-checkItem">
      {!clicked && (
        <div
          className="flexGroup pointer columnGap"
          onClick={() => {
            setClicked(true);
          }}
        >
          <Button variant="outlined">
            <Add /> Add item
          </Button>
        </div>
      )}
      {clicked && (
        <Paper style={checkItemStyle}>
          <>
            <TextField
              type="text"
              label="Enter checkItem name"
              onChange={(e) => {
                setEvent(e);
              }}
            ></TextField>
            <div className="flexGroup spaceBetween column">
              <Button
                variant="contained"
                onClick={addCheckItemButtonHandler}
                size="small"
              >
                add
              </Button>
              <Close
                className="pointer"
                onClick={() => {
                  setClicked(false);
                }}
              />
            </div>
          </>
        </Paper>
      )}
    </Grid>
  );
}
