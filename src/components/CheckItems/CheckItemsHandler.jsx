import { useEffect, useState } from "react";

import { FormGroup } from "@mui/material";

import { getAllCheckItems } from "../../TrelloAPI/TrelloAPI";
import AddNewCheckItem from "./AddNewCheckItem";
import CheckItem from "./CheckItem";
import LoadingHandler from "../ErrorAndLoading/LoadingHandler";

export default function CheckItemsHandler({ checklistID, cardID }) {
  const [allCheckItems, setAllCheckItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    getAllCheckItems(checklistID)
      .then((res) => {
        setAllCheckItems(res.data);
        setIsLoading(false);
        console.log("check items", res.data);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err.message);
        console.log(err.message);
      });
  }, []);

  return (
    (isLoading && (
      <LoadingHandler key="loadingChecklist" hType={"h7"} alignment="start" height="2rem" padding=".5rem"/>
    )) ||
    (error && (
      <div
        style={{ padding: ".5rem", color: "red" }}
        key="errorChecklistLoading"
      >
        {error}
      </div>
    )) || (
      <>
        {allCheckItems?.map((checkItem) => {
          return (
            <FormGroup key={checkItem.id}>
              <CheckItem
                allCheckItems={allCheckItems}
                setAllCheckItems={setAllCheckItems}
                checkItem={checkItem}
                checklistID={checklistID}
                cardID={cardID}
                setError={setError}
              />
            </FormGroup>
          );
        })}
        <AddNewCheckItem
          key={checklistID}
          setError={setError}
          allCheckItems={allCheckItems}
          setAllCheckItems={setAllCheckItems}
          checklistID={checklistID}
        />
      </>
    )
  );
}
