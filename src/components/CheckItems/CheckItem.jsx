import { useEffect, useState } from "react";

import { Close } from "@mui/icons-material";
import { Checkbox, FormControlLabel, IconButton } from "@mui/material";

import { deleteCheckItem, updateCheckItem } from "../../TrelloAPI/TrelloAPI";

export default function CheckItem({
  allCheckItems,
  setAllCheckItems,
  checkItem,
  checklistID,
  cardID,
  setError,
}) {
  const [isChecked, setIsChecked] = useState(false);
  const [del, setDel] = useState(true);

  useEffect(() => {
    setIsChecked(checkItem.state === "complete");
  }, []);

  function deleteCheckItemHandler() {
    deleteCheckItem(checklistID, checkItem.id)
      .then(() => {
        setAllCheckItems(
          allCheckItems.filter((item) => {
            return checkItem.id !== item.id;
          })
        );
        console.log("Deleted the checkItem: ", checkItem.name);
      })
      .catch((err) => {
        setDel(true)
        setError(err.message);
        console.log(err.message);
      });
  }

  function checkHandler(e) {
    setIsChecked(e.target.checked);
    console.log(e.target.checked);
    updateCheckItem(
      cardID,
      checkItem.id,
      e.target.checked ? "complete" : "incomplete"
    );
  }

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        width: "85%",
      }}
    >
      {
        <FormControlLabel
          control={
            <Checkbox
              checked={isChecked}
              onChange={(e) => {
                checkHandler(e);
              }}
            />
          }
          label={checkItem.name}
        />
      }
      {del && <IconButton
        color="warning"
        onClick={() => {
          deleteCheckItemHandler();
          setDel(false);
        }}
      >
        <Close />
      </IconButton>}
    </div>
  );
}
