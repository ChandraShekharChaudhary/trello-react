import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { Delete } from "@mui/icons-material";
import {
  Card,
  CardContent,
  Typography,
  Container,
  Grid,
  Button,
  Paper,
} from "@mui/material";

import {
  deleteList,
  getAllBoards,
  getBoardByID,
} from "../../TrelloAPI/TrelloAPI";
import CardHandler from "../Cards/CardsHandler";
import AddNewList from "./AddNewList";
import LoadingHandler from "../ErrorAndLoading/LoadingHandler";
import ErrorHandler from "../ErrorAndLoading/ErrorHandler";

const cardStyles = {
  display: "flex",
  flexDirection: "column",
  gap: "20px",
  backgroundColor: "#f2f2f2",
};

const cardHeader = {
  display: "flex",
  justifyContent: "space-between",
  padding: "0",
};

const boardNameStyle={
  padding:'1rem 1.7rem',
  backgroundColor: 'rgba(0, 0, 0, 0.5)',
  margin:'2px 0',
  borderRadius:'0',
  color:'#fcf0f5',
  fontSize:"1.3rem",
  fontWeight:'600',
  textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)',
}

export default function ListHandler() {
  const [allList, setAllList] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [board, setBoard] = useState({});

  let { id } = useParams();
  const navi = useNavigate();

  useEffect(() => {
    getBoardByID(id)
      .then((res) => {
        setLoading(false);
        setAllList(res.data);
      })
      .catch((err) => {
        setLoading(false);
        navi("/error");
      });

    getAllBoards()
      .then((res) => {
        setBoard(res.data.filter((board) => board.id === id)[0]);
      })
      .catch((err) => {
        setLoading(false);
        navi("/error");
      });
  }, []);

  function deleteListFromBoard(listID) {
    
    deleteList(listID)
    .then((res) => {
        let newAllList = allList.filter((list) => list.id !== listID);
        setAllList([...newAllList]);
        console.log("deleted the list: ", res.data.name);
      })
      .catch((err) => {
        setError(err.message);
        console.log(err.message);
      });
  }

  return isLoading ? (
    <LoadingHandler key="loading handler" />
  ) : (
    [
      error && <ErrorHandler key="boardError" message={error} />,
      !error && (<div className="imagePage" key="imagePage" style={{backgroundImage:`url(${board.prefs?.backgroundImage})`}}> 
        {<Paper key='boardName' sx={boardNameStyle}>{board?.name}</Paper>}
        <div
          className="detailPage"
          key="detailPage"
        >
          {allList?.map((list) => {
            return (
              <Container
                className="list"
                key={list.id}
                sx={{ minWidth: "300px", margin: "0" }}
              >
                <Card>
                  <CardContent style={cardStyles}>
                    <Container style={cardHeader}>
                      <Typography variant="h6" component="div">
                        {list.name}
                      </Typography>
                      <Button
                        color="warning"
                        onClick={() => {
                          deleteListFromBoard(list.id);
                        }}
                      >
                        <Delete />
                      </Button>
                    </Container>
                    <Grid
                      container
                      spacing={2}
                      columns={{ xs: 4, sm: 8, md: 12 }}
                    >
                      <CardHandler id={list.id} listName={list.name} />
                    </Grid>
                  </CardContent>
                </Card>
              </Container>
            );
          })}
          <AddNewList
            key="addNewList"
            id={id}
            allList={allList}
            setAllList={setAllList}
            setError={setError}
          />
        </div>
        </div>
      ),
    ]
  );
}
