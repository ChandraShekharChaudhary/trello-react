import { useState } from "react";

import {
  Button,
  Card,
  CardContent,
  Container,
  TextField,
  Typography,
} from "@mui/material";

import { createNewList } from "../../TrelloAPI/TrelloAPI";

const cardStyles = {
  display: "flex",
  flexDirection: "column",
  gap: "15px",
  backgroundColor: "#f2f2f2",
};

export default function AddNewList({ id, allList, setAllList,setError }) {
  const [newListName, setNewListName] = useState("");
  const [event, setEvent] = useState("");

  function addNewList() {
    if (newListName === "") {
      console.log("Please enter list name");
      return;
    }
    createNewList(newListName, id).then((res) => {
      event.target.value = "";
      setNewListName("");
      setAllList([...allList, res.data]);
    }).catch((err)=>{
      setError(res.message);
      console.log(res.message);
    });
  }

  return (
    <Container sx={{ minWidth: "300px", width: "300px", margin: "0" }}>
      <Card>
        <CardContent style={cardStyles}>
          <Typography variant="h6">Add New List</Typography>
          <TextField
            type="text"
            onChange={(event) => {
              setNewListName(event.target.value);
              setEvent(event);
            }}
            label="Add another list"
            placeholder="Enter list name"
          ></TextField>
          <Button
            onClick={() => {
              addNewList();
            }}
            variant="contained"
            size="small"
          >
            Add{" "}
          </Button>
        </CardContent>
      </Card>
    </Container>
  );
}
