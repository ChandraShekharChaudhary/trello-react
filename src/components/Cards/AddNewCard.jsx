import { useState } from "react";
import { Add, Close } from "@mui/icons-material";
import { Button, Grid, Paper, TextField } from "@mui/material";

import { addNewCard } from "../../TrelloAPI/TrelloAPI";

const cardStyle = {
  padding: 10,
  fontSize: 16,
  display: "flex",
  flexDirection: "column",
  gap: "10px",
};

export default function AddNewCard({ allCards, setAllCards, id,setError}) {
  const [clicked, setClicked] = useState(false);
  const [event, setEvent] = useState(null);

  function addCardButtonHandler() {
    if (event === null || event.target.value == "") {
      return;
    }

    addNewCard(event.target.value, id)
      .then((res) => {
        setAllCards([...allCards, res.data]);
        event.target.value = "";
        console.log(`Added card : ${res.data.name}`);
      })
      .catch((err) => {
        setError(err.message);
        console.log(err.message);
      });
  }

  return (
    <Grid item xs={12} key="add-new-card">
      <Paper style={cardStyle}>
        {!clicked && (
          <div
            className="flexGroup pointer columnGap"
            onClick={() => {
              setClicked(true);
            }}
          >
            <Add />
            Add Card
          </div>
        )}
        {clicked && (
          <>
            <TextField
              type="text"
              label="Enter card name"
              onChange={(e) => {
                setEvent(e);
              }}
            ></TextField>
            <div className="flexGroup spaceBetween">
              <Button
                variant="outlined"
                onClick={addCardButtonHandler}
                size="small"
              >
                Add
              </Button>
              <Close
                className="pointer"
                onClick={() => {
                  setClicked(false);
                }}
              />
            </div>
          </>
        )}
      </Paper>
    </Grid>
  );
}
