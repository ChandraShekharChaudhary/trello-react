import { useEffect, useState } from "react";

import { getAllCard } from "../../TrelloAPI/TrelloAPI";
import AddNewCard from "./AddNewCard";
import Card from "./Card";
import LoadingHandler from "../ErrorAndLoading/LoadingHandler";

export default function CardHandler({ id, listName }) {
  const [allCards, setAllCards] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    getAllCard(id)
      .then((res) => {
        setAllCards(res.data);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err.message);
        console.log(err.message);
      });
  }, []);

  return (
    (isLoading && (
      <LoadingHandler key={`${id}123`} hType={"h7"} alignment="start" />
    )) ||
    (error && (
      <div style={{ padding: "1rem", color: "red" }} key={`${id}1023`}>
        {error}
      </div>
    )) ||
    (!error && !isLoading && (
      <>
        {allCards?.map((card) => {
          return (
            <Card
              key={card.id}
              listName={listName}
              card={card}
              setError={setError}
              allCards={allCards}
              setAllCards={setAllCards}
            />
          );
        })}
        <AddNewCard
          allCards={allCards}
          key="addNewCard"
          setError={setError}
          setAllCards={setAllCards}
          id={id}
        />
      </>
    ))
  );
}
