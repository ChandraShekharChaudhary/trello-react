import { useState } from "react";
import { IconButton, Grid, Paper } from "@mui/material";
import { DeleteForever, MoreHoriz } from "@mui/icons-material";

import { deleteCard } from "../../TrelloAPI/TrelloAPI";
import CardModal from "./CardModal";

const paperStyle = {
  height: 35,
  padding: 10,
  fontSize: 16,
  display: "flex",
  justifyContent: "space-between",
  position: "relative",
  cursor: "pointer",
};

export default function Card({
  card,
  allCards,
  setAllCards,
  listName,
  setError,
}) {
  const [isMore, setIsMore] = useState(false);
  const [isModalOpen, setIsModelOpen] = useState(false);
  const [del, setDel] = useState(true);

  function deleteCardHandler() {
    deleteCard(card.id)
      .then(() => {
        let remainCards = allCards.filter(
          (eachCard) => eachCard.id !== card.id
        );
        setAllCards(remainCards);
        console.log(`Card '${card.name}' deleted`);
      })
      .catch((err) => {
        setError(err.message);
        console.log(err.message);
      });
  }

  return (
    <>
      <Grid item xs={12}>
        <Paper
          style={paperStyle}
          onClick={() => {
            setIsModelOpen(!isModalOpen);
          }}
        >
          {card.name}
          <IconButton
            size="small"
            onClick={(e) => {
              e.stopPropagation();
              setIsMore(!isMore);
            }}
          >
            <MoreHoriz className="pointer" />
          </IconButton>
          {isMore && del && (
            <div className="float">
              <IconButton
                onClick={(e) => {
                  e.stopPropagation();
                  deleteCardHandler();
                  setDel(false);
                }}
                size="small"
                color="warning"
                variant="contained"
              >
                <DeleteForever />
              </IconButton>
            </div>
          )}
        </Paper>
      </Grid>
      <CardModal
        isModalOpen={isModalOpen}
        setIsModelOpen={setIsModelOpen}
        cardName={card.name}
        listName={listName}
        cardID={card.id}
      />
    </>
  );
}
