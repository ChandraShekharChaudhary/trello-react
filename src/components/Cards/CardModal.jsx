import { FactCheckOutlined } from "@mui/icons-material";
import { Box, Modal, Typography } from "@mui/material";
import ChecklistHandler from "../Checklists/ChecklistHandler";

const style = {
  minHeight: "500px",
  color: "black",
  width: "60%",
  minWidth: "250px",
  maxWidth: "900px",
  top: "50%",
  left: "50%",
  position: "absolute",
  transform: "translate(-50%, -50%)",
  bgcolor: "white",
  boxShadow: 24,
  display: "flex",
  flexDirection: "column",
  gap: "20px",
  p: 4,
  outline: "none",
  borderRadius: 4,
  overflow:"auto",
  maxHeight:'90vh'
};

const flexDisplay = { display: "flex", flexDirection: "column", gap: "10px" };

export default function CardModal({
  isModalOpen,
  setIsModelOpen,
  cardName,
  listName,
  cardID,
}) {
  return (
    <div>
      <Modal
        sx={{}}
        open={isModalOpen}
        onClose={() => {
          setIsModelOpen(false);
        }}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Box>
            <Typography
              sx={{ display: "flex", alignItems: "center", gap: "10px" }}
              variant="h5"
            >
              <FactCheckOutlined />
              {cardName}
            </Typography>
            <Typography sx={{ marginLeft: "34px" }}>
              in list{" "}
              <span style={{ textDecoration: "underline" }}>{listName}</span>
            </Typography>
          </Box>
          <Box sx={flexDisplay}>
            <ChecklistHandler cardID={cardID} />
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
