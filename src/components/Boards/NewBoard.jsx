import { useState } from "react";

import { Card, CardContent, Button, TextField } from "@mui/material";

import { addBoard } from "../../TrelloAPI/TrelloAPI";

const cardStyles = {
  backgroundSize: "cover",
  backgroundPosition: "center",
  height: "8rem",
  width: "16rem",
  backgroundColor: "#b3dfff",
  "&:hover": {
    transform: "scale(1.005)",
  },
};

const inputStyles = {
  marginRight: "16px",
};

export default function NewBoard({ updateData }) {
  const [input, setInput] = useState("");
  const [event, setEvent] = useState("");

  function addBoardHandler() {
    if (input === "") {
      console.log("please enter the board name");
      return;
    }

    addBoard(input)
      .then((res) => {
        console.log("Board Added with name : ", res.data.name);
        updateData(res.data);
        event.target.value = "";
      })
      .catch((err) => {
        setError(err.message);
        console.log(err.message);
      });
  }

  return (
    <Card sx={cardStyles}>
      <CardContent
        sx={{ display: "flex", flexDirection: "column", gap: ".8rem" }}
      >
        <TextField
          onChange={(event) => {
            setInput(event.target.value);
            setEvent(event);
            console.log(event.target.value);
          }}
          label="Add new board"
          variant="outlined"
          placeholder="Enter board name"
          sx={inputStyles}
        />
        <Button
          sx={{ backgroundColor: "#e9e9f2" }}
          onClick={() => {
            addBoardHandler();
          }}
        >
          Add Board
        </Button>
      </CardContent>
    </Card>
  );
}
