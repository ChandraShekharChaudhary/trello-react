import { Card, CardContent, Typography } from "@mui/material";
import { Link } from "react-router-dom";

export default function Board({ board }) {
  const cardStyles = {
    backgroundImage: `url(${board.prefs.backgroundImage})`,
    backgroundColor: "#228CD5",
    backgroundSize: "cover",
    backgroundPosition: "center",
    height: "8rem",
    width: "16rem",
    '&:hover': {
      transform: 'scale(1.005)',
    }
  };

  return (
    <Link to={`/board/${board.id}`}>
      <Card sx={cardStyles}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            sx={{ color: "white", textShadow: "0px 0px 1px black",fontWeight:600 }}
          >
            {board.name}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
}
