import { Container } from "@mui/material";

import Board from "./Board";
import NewBoard from "./NewBoard";
import LoadingHandler from "../ErrorAndLoading/LoadingHandler";

export default function BoardHandler({ data, updateData,isLoading,setError }) {
  return (
    isLoading ? <LoadingHandler/>
    :<Container className="bordHandler" sx={{ display: "flex", gap: "10px", flexWrap: "wrap",paddingTop:'1rem' }}>
      {data?.map((board) => (
        <Board board={board} key={board.id} />
      ))}
      <NewBoard updateData={updateData} setError={setError}/>
    </Container>
  );
}
