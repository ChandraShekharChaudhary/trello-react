import { useEffect, useState } from "react";

import { getAllChecklists } from "../../TrelloAPI/TrelloAPI";
import AddNewChecklist from "./AddNewChecklist";
import Checklist from "./Checklist";
import LoadingHandler from "../ErrorAndLoading/LoadingHandler";

export default function ChecklistHandler({ cardID }) {
  const [allChecklist, setAllChecklist] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    getAllChecklists(cardID)
      .then((res) => {
        setAllChecklist(res.data);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err.message);
        console.log(err.message);
      });
  }, []);

  return (
    (isLoading && (
      <LoadingHandler key="loadingChecklist" hType={"h7"} alignment="start" />
    )) ||
    (error && (
      <div
        style={{ padding: "1rem", color: "red" }}
        key="errorChecklistLoading"
      >
        {error}
      </div>
    )) || (
      <>
        {[
          <AddNewChecklist
            key={"AddNewChecklist"}
            allChecklist={allChecklist}
            setAllChecklist={setAllChecklist}
            cardID={cardID}
            setError={setError}
          />,
          ...allChecklist?.map((checklist) => {
            return (
              checklist && (
                <Checklist
                  key={checklist.id}
                  cardID={cardID}
                  setError={setError}
                  checklistID={checklist.id}
                  checklistName={checklist.name}
                  allChecklist={allChecklist}
                  setAllChecklist={setAllChecklist}
                />
              )
            );
          }),
        ]}
      </>
    )
  );
}
