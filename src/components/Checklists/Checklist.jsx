import { Box, Button, Paper, Typography } from "@mui/material";
import ChecklistIcon from "@mui/icons-material/Checklist";

import { deleteChecklist } from "../../TrelloAPI/TrelloAPI";
import CheckItemsHandler from "../CheckItems/CheckItemsHandler";
import { useState } from "react";

export default function Checklist({
  checklistID,
  checklistName,
  allChecklist,
  setAllChecklist,
  cardID,
  setError,
}) {
  const [del, setDel] = useState(true);

  function deleteChecklistHandler() {
    deleteChecklist(checklistID)
      .then(() => {
        setAllChecklist(
          allChecklist.filter((checklist) => checklist.id !== checklistID)
        );
        console.log("deleted");
      })
      .catch((err) => {
        setDel(false)
        setError(err.message);
        console.log(err.message);
      });
  }
  return (
    <Box>
      <Paper
        sx={{
          padding: "10px",
          fontWeight: 600,
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: "14px",
            }}
          >
            <ChecklistIcon />
            <Typography variant="h6">{checklistName} </Typography>{" "}
          </Box>
          {del && (
            <Button
              variant="outlined"
              color="error"
              onClick={() => {
                deleteChecklistHandler();
                setDel(false);
              }}
            >
              Delete
            </Button>
          )}
        </Box>

        <CheckItemsHandler checklistID={checklistID} cardID={cardID} />
      </Paper>
    </Box>
  );
}
