import { useState } from "react";

import { Add, Close } from "@mui/icons-material";
import { Button, Grid, Paper, TextField } from "@mui/material";

import { addNewChecklist } from "../../TrelloAPI/TrelloAPI";

const cardStyle = {
  padding: 10,
  fontSize: 16,
  maxWidth: "250px",
  display: "flex",
  flexDirection: "column",
  gap: "10px",
};
export default function AddNewChecklist({
  allChecklist,
  setAllChecklist,
  cardID,
  setError,
}) {
  const [clicked, setClicked] = useState(false);
  const [event, setEvent] = useState(null);

  function addChecklistButtonHandler() {
    if (event === null || event.target.value == "") {
      return;
    }

    addNewChecklist(event.target.value, cardID)
      .then((res) => {
        setAllChecklist([...allChecklist, res.data]);
        event.target.value = "";
        console.log(`Added Checklist : ${res.data.name}`);
      })
      .catch((err) => {
        setError(err.message)
        console.log(err.message);
      });
  }

  return (
    <Grid item xs={12} key="add-new-checklist">
      <Paper style={cardStyle}>
        {!clicked && (
          <div
            className="flexGroup pointer columnGap"
            onClick={() => {
              setClicked(true);
            }}
          >
            <Add />
            Add Checklist
          </div>
        )}
        {clicked && (
          <>
            <TextField
              type="text"
              label="Enter checklist name"
              onChange={(e) => {
                setEvent(e);
              }}
            ></TextField>
            <div className="flexGroup spaceBetween">
              <Button
                variant="outlined"
                onClick={addChecklistButtonHandler}
                size="small"
              >
                add
              </Button>
              <Close
                className="pointer"
                onClick={() => {
                  setClicked(false);
                }}
              />
            </div>
          </>
        )}
      </Paper>
    </Grid>
  );
}
