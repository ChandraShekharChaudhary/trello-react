import { Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import { getAllBoards } from "./TrelloAPI/TrelloAPI";

import HeaderSection from "./components/Header/HeaderSection";
import BoardHandler from "./components/Boards/BoardHandler";
import ListHandler from "./components/Lists/ListHandler";

import "./App.css";
import ErrorHandler from "./components/ErrorAndLoading/ErrorHandler";
import { Container, Typography } from "@mui/material";

function App() {
  const [data, setData] = useState();
  const [error, setError] = useState(null);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getAllBoards()
      .then((res) => {
        console.log("Total boards are", res.data);
        setData(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setError(err.message);
        setLoading(false);
        console.log(err.message);
      });
  }, [isLoading]);

  function updateData(addedData) {
    setData([...data, addedData]);
  }

  return (
    <div className="outerContainer">
      <HeaderSection />
      {error ? (
        <ErrorHandler message={error} />
      ) : (
        <Routes>
          <Route
            path="/"
            element={
              <BoardHandler
                data={data}
                updateData={updateData}
                isLoading={isLoading}
                setError={setError}
              />
            }
          />
          <Route path="/board/:id" element={<ListHandler />} />
          <Route
            path="/error"
            element={
              <Container
                sx={{
                  textAlign: "center",
                  fontSize: "5rem",
                  height: "10rem",
                  paddingTop: "5rem",
                }}
              >
                <Typography variant="h5" color="error">
                  <span>Somthing went wrong...</span>
                </Typography>
              </Container>
            }
          />
        </Routes>
      )}
    </div>
  );
}

export default App;
