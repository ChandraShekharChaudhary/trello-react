import axios from "axios";
import Auth from "../../config";

// Board handlers
export function getAllBoards() {
  return axios.get(
    `https://api.trello.com/1/members/me/boards?&key=${Auth.key}&token=${Auth.token}`
  );
}

export function getBoardByID(id) {
  return axios.get(
    `https://api.trello.com/1/boards/${id}/lists?key=${Auth.key}&token=${Auth.token}`
  );
}

export function addBoard(name) {
  return axios.post(
    `https://api.trello.com/1/boards/?name=${name}&key=${Auth.key}&token=${Auth.token}`
  );
}

//List handler
export function createNewList(name, id) {
  return axios.post(
    `https://api.trello.com/1/lists?name=${name}&idBoard=${id}&key=${Auth.key}&token=${Auth.token}`
  );
}

export function deleteList(id) {
  return axios.put(
    `https://api.trello.com/1/lists/${id}/closed?key=${Auth.key}&token=${Auth.token}`,
    { value: true }
  );
}

//Card handlers
export function getAllCard(id) {
  return axios.get(
    `https://api.trello.com/1/lists/${id}/Cards?key=${Auth.key}&token=${Auth.token}`
  );
}

export function addNewCard(name, id) {
  return axios.post(
    `https://api.trello.com/1/cards?name=${name}&idList=${id}&key=${Auth.key}&token=${Auth.token}`
  );
}

export function deleteCard(id) {
  return axios.delete(
    `https://api.trello.com/1/cards/${id}?key=${Auth.key}&token=${Auth.token}`
  );
}

//Checklists handler
export function getAllChecklists(cardId) {
  return axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${Auth.key}&token=${Auth.token}`
  );
}

export function addNewChecklist(name, cardID) {
  return axios.post(
    `https://api.trello.com/1/checklists?name=${name}&idCard=${cardID}&key=${Auth.key}&token=${Auth.token}`
  );
}

export function deleteChecklist(checklistID) {
  return axios.delete(
    `https://api.trello.com/1/checklists/${checklistID}?key=${Auth.key}&token=${Auth.token}`
  );
}

//CheckItems handler
export function getAllCheckItems(checklistID) {
  return axios.get(
    `https://api.trello.com/1/checklists/${checklistID}/checkItems?key=${Auth.key}&token=${Auth.token}`
  );
}

export function addNewCheckItem(name, checklistID) {
  return axios.post(
    `https://api.trello.com/1/checklists/${checklistID}/checkItems?name=${name}&key=${Auth.key}&token=${Auth.token}`
  );
}

export function deleteCheckItem(checklistID, checkItemID) {
  return axios.delete(
    `https://api.trello.com/1/checklists/${checklistID}/checkItems/${checkItemID}?key=${Auth.key}&token=${Auth.token}`
  );
}

//Checkbox handler
export function updateCheckItem(cardID, checkItemID, isState) {
  return axios.put(
    `https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?key=${Auth.key}&token=${Auth.token}`,
    { state: `${isState}` }
  );
}
